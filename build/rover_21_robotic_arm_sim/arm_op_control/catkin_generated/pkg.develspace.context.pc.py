# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/itu-rover/rover_21_workspace/src/rover_21_robotic_arm_sim/arm_op_control/include".split(';') if "/home/itu-rover/rover_21_workspace/src/rover_21_robotic_arm_sim/arm_op_control/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-larm_op_control".split(';') if "-larm_op_control" != "" else []
PROJECT_NAME = "arm_op_control"
PROJECT_SPACE_DIR = "/home/itu-rover/rover_21_workspace/devel"
PROJECT_VERSION = "0.0.0"
