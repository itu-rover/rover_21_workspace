# CMake generated Testfile for 
# Source directory: /home/itu-rover/rover_21_workspace/src
# Build directory: /home/itu-rover/rover_21_workspace/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("rover_21_robotic_arm_sim/arm21_moveit_config")
subdirs("rover_21_localization")
subdirs("rover_21_cv_artag-master/ar_tag_demo")
subdirs("rover_21_robotic_arm_sim/arm_op_control")
subdirs("rover_21_robotic_arm_sim/arm_serial")
subdirs("d435_rtabmap")
subdirs("rover_20_serial")
subdirs("rover_21_navigation")
subdirs("rover_20_imu")
subdirs("darknet_ros/darknet_ros/darknet_ros_msgs")
subdirs("cv_camera")
subdirs("darknet_ros/darknet_ros/darknet_ros")
subdirs("rover_20_control")
subdirs("rover_21_control")
subdirs("rover_21_robotic_arm_sim/arm_21_gazebo")
subdirs("bio_ik")
