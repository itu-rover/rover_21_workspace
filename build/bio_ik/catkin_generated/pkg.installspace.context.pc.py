# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "eigen_conversions;moveit_core;moveit_ros_planning;pluginlib;roscpp;tf2;tf2_kdl;tf2_geometry_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lbio_ik".split(';') if "-lbio_ik" != "" else []
PROJECT_NAME = "bio_ik"
PROJECT_SPACE_DIR = "/home/itu-rover/rover_21_workspace/install"
PROJECT_VERSION = "1.0.0"
